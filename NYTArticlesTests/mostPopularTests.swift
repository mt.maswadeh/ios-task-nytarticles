//
//  mostPopularTests.swift
//  NYTArticlesTests
//
//  Created by Mohammad on 7/3/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import XCTest
import Alamofire
@testable import NYTArticles

class mostPopularTests: XCTestCase {
    
    var viewControllerUnderTest: MostPopularViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.viewControllerUnderTest = storyboard.instantiateViewController(withIdentifier: "MostPopularViewController") as? MostPopularViewController
                self.viewControllerUnderTest.loadView()
        self.viewControllerUnderTest.viewDidLoad()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testHasATableView() {
        XCTAssertNotNil(viewControllerUnderTest.articlesTableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(viewControllerUnderTest.articlesTableView.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDelegate.self))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:didSelectRowAt:))))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(viewControllerUnderTest.articlesTableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewCellHasReuseIdentifier() {
        let cell = viewControllerUnderTest.tableView(viewControllerUnderTest.articlesTableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? ArticleCell
        let actualReuseIdentifer = cell?.reuseIdentifier
        let expectedReuseIdentifier = "ArticleCell"
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
    }

}
