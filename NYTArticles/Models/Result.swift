//
//  Result.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation

struct Results:Codable {
    
    var url:String?
    var adxKeywords:String?
    var column:String?
    var section:String?
    var byline:String?
    var type:String?
    var title:String?
    var abstract:String?
    var publishedDate:String?
    var source:String?
    var id:Int?
    var assetId:Int?
    var views:Int?
    var desFacet:[String]?
    var orgFacet:[String]?
    var perFacet:[String]?
    var geoFacet:[String]?
    var media:[Media]?

    private enum CodingKeys : String, CodingKey {
        case url,column,section,byline,type,title,abstract,source,id,views,media
        case adxKeywords = "adx_keywords"
        case publishedDate = "published_date"
        case assetId = "asset_id"
        case desFacet = "des_facet"
        case orgFacet = "org_facet"
        case perFacet = "per_facet"
        case geoFacet = "geo_facet"

    }
 
}

