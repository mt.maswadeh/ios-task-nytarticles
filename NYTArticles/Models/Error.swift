//
//  Error.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation

struct Error:Codable {
    var code:Int?
    var description:String?
}

