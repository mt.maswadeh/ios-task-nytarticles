//
//  MainResponse.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation

struct MainResponse:Codable {
    
    var status:String?
    var copyright:String?
    var numResults:Int?
    var results:[Results]?
    
    private enum CodingKeys : String, CodingKey {
        case status,copyright,results
        case numResults = "num_results"
    }
    
}
