//
//  DefaultResponse.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation

protocol ResponseStatus:Codable {
    var isSuccess:Bool? { get set }
    var errors:[Error]? { get set }
}

struct DefaultResponse:ResponseStatus {
    var isSuccess:Bool?
    var errors:[Error]?
}
