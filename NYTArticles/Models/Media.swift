//
//  Media.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation

struct Media:Codable {
    
    var type:String?
    var subtype:String?
    var caption:String?
    var copyright:String?
    var approvedForSyndication:Int?
    var mediaMetadata:[MetadataMedia]?
    
    private enum CodingKeys : String, CodingKey {
        case type,subtype,caption,copyright
        case approvedForSyndication = "approved_for_syndication"
        case mediaMetadata = "media-metadata"
    }
    
}

struct MetadataMedia:Codable {
    
    var url:String?
    var format:String?
    var height:Int?
    var width:Int?

}

