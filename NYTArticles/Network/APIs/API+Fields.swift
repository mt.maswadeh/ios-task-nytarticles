//
//  API+Fields.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation
import Alamofire

extension API {
  
    var method : HTTPMethod {
        switch self {
        case .popularArticles:
            return HTTPMethod.get
        }
    }

    
    var header : [String:String]? {
        switch self {
        case .popularArticles:
            return ["Content-type": "application/x-www-form-urlencoded"]
    }
}
    
    var parameter : [String:Any]? {
        switch self {
        case .popularArticles:
            return nil
        }
    }
    
    var files : [APIFile]? {
        switch self {
        default:
            return nil
        }
    }
}

