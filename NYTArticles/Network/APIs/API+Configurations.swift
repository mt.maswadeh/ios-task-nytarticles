//
//  API+Configurations.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    
    var parameterEncoding : ParameterEncoding {
        switch self {
        default:
            return URLEncoding.default
        }
    }
    
    var authenticate : URLCredential? {
        switch self {
        default:
            return nil
        }
    }
    
    var timeout : TimeInterval? {
        switch self {
        default:
            return 120
        }
    }
    
    var ignorCache : Bool {
        switch self {
        default:
            return true
        }
    }
    
    var skipInvalidCertificate : Bool {
        switch self {
        default:
            return true
        }
    }
    
}
