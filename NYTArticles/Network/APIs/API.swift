//
//  API.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation
import Alamofire

enum API {
    static let domain = "http://api.nytimes.com/svc"
    
    case popularArticles(section:String,period:String)
    
    var route : String {
        var url = API.domain
        switch self {
        case .popularArticles:
            url = url.appending("/mostpopular/v2/mostviewed/")
            
        }
        return url
    }
    
    var url : URL? {
        var url = route
        switch self {
   
        case .popularArticles(let section, let period):
            url = url.appending("\(section)/\(period).json?api-key=\(apiKey)")
      
        }
        return URL.init(string: url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "")
    }
    
}
