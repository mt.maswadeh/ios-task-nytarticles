//
//  Connectivity.swift
//  NYTArticles
//
//  Created by Mohammad on 7/3/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

