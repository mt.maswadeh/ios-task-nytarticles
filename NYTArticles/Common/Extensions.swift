//
//  Extensions.swift
//  NYTArticles
//
//  Created by Mohammad on 7/3/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation
import FFGlobalAlertController

extension UIViewController {
    
    //MARK:- Alert
    func presentAlert(_ title: String, message : String? = nil, callBack:(()->())? = nil) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { (action:UIAlertAction) in
            callBack?()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIColor {
    static let themeColor = UIColor(red:71, green:228, blue:194, alpha:1.0)
    
    
}

extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2) //instead of let radius = CGRectGetWidth(self.frame) / 2
        self.layer.masksToBounds = true
    }
}
