//
//  Dialogs.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation
import KVNProgress

class Dialogs {
    
    static func showAlert(_ title: String, message : String? = nil, callBack:(()->())? = nil) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { (action:UIAlertAction) in
            callBack?()
        }))
        alert.show()
    }
    
    static func showConfirmationAlert(_ title: String, message : String,
                                      confirmTitle : String? = NSLocalizedString("Yes", comment: "Yes"),
                                      cancelTitle : String? = NSLocalizedString("No", comment: "No"),
                                      confirmCallBack:@escaping (()->()),
                                      cancelCallBack:@escaping (()->())) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: confirmTitle, style: .default, handler: { (action:UIAlertAction) in
            confirmCallBack()
        }))
        alert.addAction(UIAlertAction.init(title: cancelTitle, style: .cancel, handler: { (action:UIAlertAction) in
            cancelCallBack()
        }))
        alert.show()
    }
    
    // MARK:- KVNProgress
    static func loadingDialogConfiguration(){
        let configuration = KVNProgressConfiguration.init()
        
        configuration.statusColor = UIColor.themeColor
        configuration.statusFont = UIFont.systemFont(ofSize: 13)
        configuration.circleStrokeForegroundColor = UIColor.themeColor
        configuration.circleStrokeBackgroundColor = UIColor.themeColor.withAlphaComponent(0.3)
        configuration.backgroundFillColor = UIColor.white.withAlphaComponent(0.9)
        configuration.backgroundTintColor = UIColor.white.withAlphaComponent(1)
        configuration.successColor = UIColor.themeColor
        configuration.errorColor = UIColor.themeColor
        configuration.stopColor = UIColor.themeColor
        configuration.minimumDisplayTime = 0.3
        configuration.minimumSuccessDisplayTime = 3.0
        configuration.minimumErrorDisplayTime = 15.0
        configuration.isFullScreen = false
        configuration.doesShowStop = false
        configuration.stopRelativeHeight = 0.4
        
        configuration.tapBlock = { progressView in
            if progressView?.style != KVNProgressStyle.progress {
                DispatchQueue.main.sync {
                    KVNProgress.dismiss()
                }
            }
        }
        KVNProgress.setConfiguration(configuration)
    }
    
    static func dismiss(){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
        }
    }
    
    static func showLoading(_ message:String? = nil){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
            if let text = message {
                KVNProgress.show(withStatus: text)
            }else {
                KVNProgress.show()
            }
        }
    }
    
    static func showSuccess(_ message:String? = nil){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
            if let text = message {
                KVNProgress.showSuccess(withStatus: text)
            }else {
                KVNProgress.showSuccess()
            }
        }
    }
    
    static func showError(_ message:String? = nil){
        DispatchQueue.main.async {
            KVNProgress.dismiss()
            if let text = message {
                KVNProgress.showError(withStatus: text)
            }else {
                KVNProgress.showError()
            }
        }
    }
}

