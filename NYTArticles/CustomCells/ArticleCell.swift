//
//  ArticleCell.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import UIKit
import SDWebImage
class ArticleCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var byLineLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnailImageView.setRounded()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fill(_ results:Results) {
        
        titleLabel.text = results.title
        byLineLabel.text = results.byline
        typeLabel.text = results.type
        dateLabel.text = results.publishedDate

        thumbnailImageView.sd_setImage(with: URL(string: results.media?[0].mediaMetadata?[1].url ?? ""), placeholderImage: UIImage(named: "ic_small_logo"))

    }
    
}
