//
//  MostPopularViewPresenter.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation
import Alamofire

protocol MostPopularViewPresenter : NSObjectProtocol{
    func mostPopularRequestSuccessful(_ articles:[Results])
    func mostPopularRequestFailed(_ error:String)
}

final class MostPopularPresenter {
    weak fileprivate var viewPresenter : MostPopularViewPresenter?
    
    init(_ view:MostPopularViewPresenter) {
        viewPresenter = view
    }
    
    func getMostPopularArticles(section:String,period:String) {
        Dialogs.showLoading()
        APIClient.request(api: API.popularArticles(section:section, period: period)) { [weak self] (getArticles:MainResponse?, response:DataResponse<Data>?, cache:APIClient.DataStatus) in
            
            Dialogs.dismiss()
            guard let `self` = self else {
                return
            }
            if let articles = getArticles,
                let articlesList = articles.results{
                self.viewPresenter?.mostPopularRequestSuccessful(articlesList)
            }else {
                self.viewPresenter?.mostPopularRequestFailed(NSLocalizedString("Internel server error", comment: "Server error"))
            }
        }
        
    }
    

}
