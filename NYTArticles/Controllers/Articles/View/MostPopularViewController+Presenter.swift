//
//  MostPopularViewController+Presenter.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import Foundation

extension MostPopularViewController : MostPopularViewPresenter {
    
    func mostPopularRequestSuccessful(_ articles: [Results]) {
        
        self.resultsArray = articles
        self.articlesTableView.reloadData()
    }
    
    func mostPopularRequestFailed(_ error: String) {
        
        Dialogs.showAlert(error)

    }
    
}
