//
//  MostPopularViewController.swift
//  NYTArticles
//
//  Created by Mohammad on 7/2/19.
//  Copyright © 2019 SmartDubai. All rights reserved.
//

import UIKit

class MostPopularViewController: BaseViewController {

    var resultsArray = [Results]()
    lazy var presenter = MostPopularPresenter(self)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var articlesTableView: UITableView!{
        didSet{
            self.articlesTableView.dataSource = self
            self.articlesTableView.delegate = self
            self.articlesTableView.register(UINib(nibName: "ArticleCell", bundle: nil), forCellReuseIdentifier: "ArticleCell")
            self.articlesTableView.estimatedRowHeight = 70
            if Connectivity.isConnectedToInternet {
                self.presenter.getMostPopularArticles(section: "all-sections", period: "7")
            }
            else {
                Dialogs.showAlert(connectionMessage)
            }
        }
    }

}
